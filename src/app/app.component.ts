import { Component } from '@angular/core';
import { SessionService } from "./services/session.service";
import { Subscription } from 'rxjs';
import { Router } from '@angular/router';


@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss'],
})
export class AppComponent {
  user: {
    foto: string;
    nombre: string
    telefono: number
  } = { foto: "", nombre: "", telefono: 0 }
  public appPages = [
    { title: 'Login', url: '/auth/login', icon: 'person' },
    { title: 'Registro', url: '/auth/register', icon: 'person-add' },

  ];
  public labels = [];
  subscription: Subscription;

  constructor(private sessionService: SessionService, private router: Router) {

  }
  ngOnInit(): void {

    this.subscription = this.sessionService.currentUser.subscribe((user: any) => {
      if (user?.nombre) {
        this.router.navigateByUrl('/home');
        this.user = user

        this.appPages = [
          { title: 'Home', url: '/home', icon: 'home' },
          { title: 'About', url: '/home/about', icon: 'accessibility' },


        ]
      } else {
        this.user = { foto: "", nombre: "", telefono: 0 }

        this.appPages = [
          { title: 'Login', url: '/auth/login', icon: 'person' },
          { title: 'Registro', url: '/auth/register', icon: 'person-add' },
        ]
      }
    })
    this.sessionService.checkuser()

  }
  logout() {
    this.sessionService.destroy()
  }
  ngOnDestroy() {
    this.subscription.unsubscribe();
  }
}
