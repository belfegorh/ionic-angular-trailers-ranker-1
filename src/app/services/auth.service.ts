import { Injectable } from '@angular/core';
import { StorageService } from "./storage.service";
import { SessionService } from "./session.service";
import { FingerprintAIO } from '@ionic-native/fingerprint-aio/ngx';
import { ToastController } from '@ionic/angular';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor(private st: StorageService, private sessionService: SessionService,
    public faio: FingerprintAIO,
    public toastController: ToastController) { }



  async login(payloadLogin) {
    const userData = { ...payloadLogin }
    let users = await this.st.getUsers()
    const login = users.find((user: any) => {
      return user.nombre.trim() === userData.nombre.trim() && user.password.trim() === userData.password.trim()
    })
    console.log(login)
    if (login) {
      this.sessionService.changeUser(login)
      this.presentToast("bienvenido","success")
      return true
    } else {
      this.presentToast("Usuario y/o password incorrectos","warning")

      return false
    }

  }
  async registerUser(payloadRegister) {
    const userData = { ...payloadRegister }
    let users = await this.st.getUsers()
    const login = users.find((user: any) => {
      return user.nombre.trim() === userData.nombre.trim() && user.password.trim() === userData.password.trim()
    })
    console.log(login)
    if (login?.nombre) {

      this.presentToast("El usuario ya existe","warning")

      return false
    } else {
      const newUster = await this.st.setUser(userData)

      this.presentToast("Usuario registrado correctamente","success")
      return true
     
    }

  }

  registerTouch(key) {
    this.faio.registerBiometricSecret({
      description: "Some biometric description",
      secret: key,
      invalidateOnEnrollment: true,
      disableBackup: true, // always disabled on Android
    }).then(result => {
      console.log(120, result);
    }).catch(err => {
      console.log(122, err);
    });
  }
  load() {
    this.faio.loadBiometricSecret({
      description: "Some biometric description",
      disableBackup: true, // always disabled on Android
    }).then(async result => {
      let users = await this.st.getUsers()
      const login = users.map((user: any) => {
        if (user.nombre.trim() === result.trim()) {
          this.sessionService.changeUser(user)
          return true
        } else {
          return false
        }
      })
      console.log(120, result);
    }).catch(err => {
      this.presentToast("No hay huella registrada","warning")
      console.log(122, err);
    });
  }
  async check() {
    return this.faio.isAvailable().then(result => {
      console.log(result);
      return true
    }).catch(err => {
      console.log(err);
      return false
    });
  }
  async presentToast(message,color) {
    const toast = await this.toastController.create({
      message: message,
      duration: 2000,
      color:color
    });
    toast.present();
  }

}
