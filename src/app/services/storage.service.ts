import { Injectable } from '@angular/core';

import { Storage } from '@capacitor/storage';
import { Platform } from '@ionic/angular';



@Injectable({
  providedIn: 'root',
})
export class StorageService {


  constructor(private platform: Platform) { }


  setName = async (name, value) => {
    const savedItem = await Storage.set({
      key: name,
      value: JSON.stringify(value),
    });
    return savedItem
  };

  checkName = async (name) => {
    const { value } = await Storage.get({ key: name });
    return value
  };

  removeName = async (name) => {
    await Storage.remove({ key: name });
  };
  listKeys = async () => {
    const keys = await Storage.keys()
    return keys
  }

  setUser = async (user)=>{
    let users = await this.getUsers()
    let arrayUser = [user]
    const arrayNewUsers = [...users,...arrayUser]
    this.setName("users",arrayNewUsers)
  }
  getUsers = async () => {
    const users = await this.checkName("users")
    if(users){
      return JSON.parse(users)
    }else{
      return []
    }
  }






}

