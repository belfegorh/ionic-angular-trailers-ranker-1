import { Injectable } from '@angular/core';
import { SessionService } from "./session.service";

export interface Movie {
  id:number;
  titulo: string;
  descripcion: string;
  image: string;
  src: string;
}

@Injectable({
  providedIn: 'root'
})
export class DataService {
  
  public movies: {terror:Movie[],sifi:Movie[],anime:Movie[],} = {
    terror: [
      {id:0, titulo: "exorcismo en el septimo dia", 
      descripcion: "Un reconocido exorcista se reúne con un sacerdote novicio en su primer día de entrenamiento. A medida que descubren el infierno que existe sobre la Tierra, las líneas entre el bien y el mal se distorsionan, dando lugar a que emerjan sus propios demonios.", image: "assets/images/movie-trailers/exorcismo-en-el-septimo-dia-the.jpg", 
      src: "https://www.youtube.com/embed/GW7PmCiBQUU" },
      {id:1, titulo: "The Jack In The Box 2", 
      descripcion: "Cuando el antiguo muñeco de Jack-in-the-box es sacado de la tierra y se abre, sus nuevos propietarios pronto tienen razones para creer que el espeluznante muñeco de payaso tiene vida propia.", image: "assets/images/movie-trailers/the-jack-in-the-box.jpg", 
      src: "https://www.youtube.com/embed/_M2sQYGnlrs" },

      {id:2, titulo: "Antlers", 
      descripcion: "la profesora de un pequeño pueblo de Oregón y su hermano, el sheriff local, descubren que un joven alumno esconde un secreto peligroso con consecuencias aterradoras.", image: "assets/images/movie-trailers/antlers.jpg", 
      src: "https://www.youtube.com/embed/5PHMpW3wm9w" },
    ],
    sifi: [
      {id:0, titulo: "DUNE", descripcion: "Arrakis, también denominado 'Dune', se ha convertido en el planeta más importante del universo. A su alrededor comienza una gigantesca lucha por el poder que culmina en una guerra interestelar.", image: "assets/images/movie-trailers/dune.jpg", 
      src: "https://www.youtube.com/embed/mExBLAUyU0Q" },
      {id:1, titulo: "VENOM 2: Let There Be Carnage", descripcion: "Después de encontrar un cuerpo anfitrión en el periodista de investigación Eddie Brock, el simbionte alienígena debe enfrentarse a un nuevo enemigo, Carnage, el alter ego del asesino en serie Cletus Kasady.", image: "assets/images/movie-trailers/venom2.jpg", 
      src: "https://www.youtube.com/embed/Dbln5UJz5bU" },

      {
       id:2, titulo: "SHANG-CHI ",
        descripcion: "Shang-Chi y la Leyenda de los Diez Anillos de Marvel Studios está protagonizada por Simu Liu como Shang-Chi, quien debe enfrentarse al pasado que creía haber dejado atrás cuando se ve envuelto en la red de la misteriosa organización de los Diez Anillos.",
        image: "assets/images/movie-trailers/shanshi.jpg", 
        src: "https://www.youtube.com/embed/1pR5Gcc56wk"
      },

    ],
    anime: [
      
      {id:0, titulo: "Kimetsu no Yaiba Season 2 ", 
      descripcion: "La Temporada 2 de Kimetsu no Yaiba nos narrará el Arco del Distrito Rojo; también conocido como Arco del Distrito del Entretenimiento, que recoge 30 capítulos del manga (del 70 al capítulo 99).", 
      image: "assets/images/movie-trailers/kimetsu.jpg", 
      
      src: "https://www.youtube.com/embed/2MKkj1DQ0NU" },    
      {id:1, titulo: "STAR WARS: Visions", 
      descripcion: "Star Wars: Visions será una serie de cortometrajes animados que celebran la galaxia de Star Wars a través de la lente de los mejores creadores de anime del mundo. Esta colección de antología traerá diez visiones fantásticas de varios de los principales estudios de anime japoneses, ofreciendo una perspectiva cultural fresca y diversa de Star Wars", 
      image: "assets/images/movie-trailers/star-wars-visions.jpg", 
      src: "https://www.youtube.com/embed/wcPIvcZTgi4" },  
      {id:2, titulo: "EVANGELION: 3.0+1.01 THRICE UPON A TIME ", 
      descripcion: "En París, un equipo de la organización Wille, liderado por Maya Ibuki, trabaja en un sistema diseñado para restaurar la ciudad a su estado anterior. Al ser atacados por las fuerzas de Nerv, son defendidos por la flota Wunder y Mari Illustrious Makinami en la Unidad-08. Mari derrota a los atacantes y el equipo de Wille restaura París. Mientras tanto, Asuka Shikinami Langley, Rei Ayanami y Shinji Ikari, todavía abatidos, caminan por las afueras de Tokio-3.", 
      image: "assets/images/movie-trailers/evangelion.jpg", 
      src: "https://www.youtube.com/embed/GZfuWMDEJpw" },
    ]
  }

  constructor(private session:SessionService) { }

  public getMovies(): {terror:Movie[],sifi:Movie[],anime:Movie[],}{
    return this.movies;
  }

  public getMmovieById(genere,id: number): Movie {
    return this.movies[genere][id];
  }

}
