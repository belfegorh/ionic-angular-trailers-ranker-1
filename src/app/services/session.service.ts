import { Injectable, Output } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { StorageService } from "./storage.service";
import { FingerprintAIO } from '@ionic-native/fingerprint-aio/ngx';

@Injectable({
  providedIn: 'root'
})
export class SessionService {

  private user = new BehaviorSubject({});
  currentUser = this.user.asObservable();
  constructor(private storage:StorageService, public faio: FingerprintAIO,) { }
  
  async checkuser(){
    const user = await this.storage.checkName("user")
    this.user.next(JSON.parse(user))
  }
  changeUser(newUser: any) {
    this.user.next(newUser)
    this.storage.setName("user",newUser)
  }
  async destroy(){
    const vacio = {}
    await this.storage.removeName("user")
    this.user.next(vacio)

  }
  async register(key) {
    console.log('register');
    this.faio.registerBiometricSecret({
      description: "Some biometric description",
      secret: key,
      invalidateOnEnrollment: true,
      disableBackup: true, // always disabled on Android
    }).then(result => {
      console.log(120, result);
    }).catch(err => {
      console.log(122, err);
    });
  }
}
