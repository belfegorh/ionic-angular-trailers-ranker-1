import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IonicModule } from '@ionic/angular';
import { FormsModule } from '@angular/forms';

import { HomePage } from './home.page';
import { HomePageRoutingModule } from './home-routing.module';
import { AboutComponent } from "./about/about.component";
import { ViewMovieComponent } from "./view-movie/view-movie.component";
import {MatExpansionModule} from '@angular/material/expansion';


@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    HomePageRoutingModule,
    MatExpansionModule
  ],
  declarations: [HomePage,AboutComponent,ViewMovieComponent]
})
export class HomePageModule {}
