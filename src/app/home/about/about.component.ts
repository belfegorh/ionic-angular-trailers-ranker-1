import { Component, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { SessionService } from "../../services/session.service";
import { AuthService } from "../../services/auth.service";


@Component({
  selector: 'app-about',
  templateUrl: './about.component.html',
  styleUrls: ['./about.component.scss'],
})
export class AboutComponent implements OnInit {
  user: any = { foto: "", nombre: "", telefono: 0 }
  subscription: Subscription;
  image: string = ""
  fingerprint:any = false


  constructor(private sessionService: SessionService, private authService:AuthService) {
    this.subscription = this.sessionService.currentUser.subscribe((user: any) => {
      if (user?.nombre) {
        this.user = user
        this.image = `data:image/png;base64,${user.foto.base64String}`
      }
    })
  }

  ngOnInit() { 
    this.checkFingerprint()
  }
  async registrar() {
    const registrarhuella = this.sessionService.register(this.user.nombre)
  }
  async checkFingerprint(){
    this.fingerprint = await this.authService.check()
  }
}
