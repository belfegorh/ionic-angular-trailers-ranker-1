import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { DataService, Movie } from '../../services/data.service';
import { DomSanitizer } from '@angular/platform-browser';
import { SessionService } from "../../services/session.service";
import { StorageService } from "../../services/storage.service";


@Component({
  selector: 'app-view-movie',
  templateUrl: './view-movie.component.html',
  styleUrls: ['./view-movie.component.scss'],
})
export class ViewMovieComponent implements OnInit {
  id = this.activatedRoute.snapshot.paramMap.get('id');
  genere = this.activatedRoute.snapshot.paramMap.get('gener');
  user

  public movie: Movie;
  movieStars = []
  index: number = -1;

  safeURL
  constructor(
    private data: DataService,
    private activatedRoute: ActivatedRoute,
    private _sanitizer: DomSanitizer,
    private session: SessionService,
    private storage: StorageService
  ) {
    this.movieStars = [1, 2, 3, 4, 5];
  }

  async ngOnInit() {

    this.movie = this.data.getMmovieById(this.genere, parseInt(this.id, 10));
    this.safeURL = this._sanitizer.bypassSecurityTrustResourceUrl(this.movie.src);
    this.user = await this.getUserName();
    this.getStars(this.user)
  }

  onClickItem(index) {
    this.index = index;
    this.setStars(index)
  }

  getBackButtonText() {
    const win = window as any;
    const mode = win && win.Ionic && win.Ionic.mode;
    return mode === 'ios' ? 'Home' : '';
  }
  async getUserName() {
    const user: any = await this.storage.checkName("user")
    return JSON.parse(user).nombre

  }
  async getStars(user) {
    const moviekey:string = `${this.user}/${this.genere}/${this.id}`

    const allstars: any = await this.storage.checkName(moviekey)
    if (allstars) {
      this.index=allstars
    }
  }
  async setStars(index) {
    const moviekey:string = `${this.user}/${this.genere}/${this.id}`
    await this.storage.setName(moviekey, index) 
    
  }
}
