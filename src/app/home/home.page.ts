import { Component } from '@angular/core';
import { DataService, Movie } from '../services/data.service';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {
  panelOpenState = false;
  terrorConrol = this.collapsed()
  sifiConrol = this.collapsed()
  animeConrol = this.collapsed()


  constructor(private data: DataService) { }
 

  getMovies(): { terror: Movie[], sifi: Movie[], anime: Movie[], } {
    return this.data.getMovies();
  }
  collapsed(): { change: any,value:any,name:any } {
    let state = false;
    const change = () => {
      state = !state
    }
    return {
      change: function () {
        change();
      },
      value: function() {
        return state;
      },
      name: function() {
        const name = state?"caret-back":"caret-down"
        return name;
      }
    }
  }

  slideOpts = {
    initialSlide: 1,
    speed: 400
  };
}
