import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { HomePage } from './home.page';
import { AboutComponent } from "./about/about.component";
import { ViewMovieComponent } from "./view-movie/view-movie.component";
const routes: Routes = [
  {
    path: '',
    component: HomePage
  },
  {
    path: 'about',
    component: AboutComponent
  },
  {
    path: 'movie/:gener/:id',
    component: ViewMovieComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class HomePageRoutingModule {}
