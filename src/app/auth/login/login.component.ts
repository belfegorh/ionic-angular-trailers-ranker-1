import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AnimationBuilder, IonicSafeString, SpinnerTypes } from '@ionic/angular';
import { AuthService } from "../../services/auth.service";

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
})
export class LoginComponent implements OnInit {
  fingerprint:any = false

  constructor(private formBuilder: FormBuilder, public authService:AuthService,
   ) { }
  image: any = ""
  public formLogin: FormGroup

  ngOnInit() {
    this.formLogin = this.formBuilder.group({
      nombre: ['',
        [
          Validators.required,
          Validators.minLength(6)
        ]
      ],

      password: ['',
        [
          Validators.required,
          Validators.minLength(6)
        ]
      ]
    })
    this.checkFingerprint()
  }
  async send() {
    const userData = { ...this.formLogin.value }
    const response = await this.authService.login(userData)
    console.log(response)
  
  }
  async checkFingerprint(){
    this.fingerprint = await this.authService.check()
  }

  async loginFingerPrint(){
    await this.authService.load()
  }




}
