import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Camera, CameraResultType, CameraSource, Photo } from '@capacitor/camera';
import { LoadingController } from '@ionic/angular';
import { ToastController } from '@ionic/angular';

import { AuthService } from "../../services/auth.service";
import { AnimationBuilder, IonicSafeString, SpinnerTypes } from '@ionic/angular';

import { FingerprintAIO } from '@ionic-native/fingerprint-aio/ngx';


@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss'],
})
export class RegisterComponent implements OnInit {

  constructor(private authService: AuthService, 
    private formBuilder: FormBuilder,
    public loadingController: LoadingController,
    public toastController: ToastController,
    ) { }
  image: any =""
  messages=""
  public formRegistro: FormGroup

  ngOnInit() {
    this.formRegistro = this.formBuilder.group({
      nombre: ['',
        [
          Validators.required,
          Validators.minLength(6)
        ]
      ],
      telefono: ['',
        [
          Validators.required,
          Validators.minLength(6)
        ]
      ],

      password: ['',
        [
          Validators.required,
          Validators.minLength(6)
        ]
      ]
    })
  }

  async presentToast(mesagge) {
    const toast = await this.toastController.create({
      message: mesagge,
      duration: 2000,
      color:"success",
      position:"bottom"
    });
    toast.present();
  }
  async presentLoading() {
    const loading = await this.loadingController.create({
      message: 'Please wait...',
      duration: 500
    });
    await loading.present();
  }


  async send() {
    const userData = {...{foto:this.image},...this.formRegistro.value}
    await  this.presentLoading()

    await this.authService.registerUser(userData)
  }

  

  takePicture = async () => {
    this.image = await Camera.getPhoto({
      quality: 90,
      allowEditing: false,
      resultType: CameraResultType.Base64,
      source: CameraSource.Camera,
    });

    // image.webPath will contain a path that can be set as an image src.
    // You can access the original file using image.path, which can be
    // passed to the Filesystem API to read the raw data of the image,
    // if desired (or pass resultType: CameraResultType.Base64 to getPhoto)
    var imageUrl = this.image.webPath;
    // Can be set to the src of an image now
    // imageElement.src = imageUrl;
  };

  
}
