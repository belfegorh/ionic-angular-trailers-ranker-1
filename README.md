# ionic-angular-trailers-ranker-1

  

app para calificar trailers de películas, usando el storage para simular datos

  

## 1. Registro

  

- a. Los campos a solicitar son:

i. Nombre completo

ii. Número de contacto

iii. Registro de Foto

iv. Password

v. Touch ID

- b. Los campos ingresados deben guardarse en el

storage del dispositivo, donde se deberá estructurar

dicha información en formato JSON, homologando a

lo que podría llegar a ser un request del envío de la

información, para esto la foto deberá ser manejada

como base64.

## 2. Login

- a. Se ingresa con password o touch ID, de igual manera

se debe estructurar dicha información en formato

JSON, simulando el request a enviar en caso de

existir un Back-end, donde la validación se hará con

la data guardada previamente en el registro.

## 3. Home

- a. En el home deberá existir un menú de inicio donde se

muestre la información de la persona que se logueo

- b. El home al ser una aplicación de contenido, deberá

mostrar por clasificación de género, trailers de las

películas a estrenar en el 2021.

- c. El listado de películas por género podrá ocultarse o

no, según el usuario lo desee.

- d. Al dar click a una película, deberá llevar a una nueva

pantalla que tenga una breve reseña de la misma,

donde si se le permitirá dar click para ver el trailer

completo.

- e. Cada película deberá ser habilitada por un link de

youtube y deberá permitir pantalla completa o no,

dentro de la misma app.

- f. Cada película deberá contar con una calificación,

basada en estrellas, 5 estrellas es muy buena, 1

estrella es muy mala. Dichas calificaciones deberán

ser guardadas en el storage del dispositivo,

relacionado con el usuario.

- g. Opcional: cada película podrá compartirse a los

contactos por whatsapp como recomendación.